# pokeApp

## Getting started

This application contains a list of Pokemon with details and their evolution. The data on this application is taken from https://pokeapi.co/docs/v2.

My main project is on the master branch. This application has 1 MainActivity as a container for 2 fragments in it, namely HomeFragment and DetailFragment. Then in the Detail Fragment I use tablayout and viewpager2 to build 2 fragment tabs namely DetailStatsFragment and DetailEvolutionFragment. The architecture applied to this program is MVVM which is a clean architecture recommended by Google for developing android applications.

To get data from the provided API, I use retrofit2 and rxJava. Then to give the user a better experience, I also do caching pokemon list and their stat detail by storing it in the room db. This is expected so that the user does not wait long to load the same data as the data he has previously loaded. Then I also don't want a force close caused by an internet connection, therefore with the help of retrofit2 this application already has error handling if the user opens it without an internet connection or when there is a problem with the API.

A feature that does not yet exist and needs to be added to this application in my opinion is the search feature. This feature will make it easier for users to find the desired pokemon quickly without scrolling. That's all from me, hope you enjoy it. Thank you in advance.
